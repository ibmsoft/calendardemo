//
//  ViewController.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoodStuffIOS/GoodStuffIOS.h>

@interface ViewController : UIViewController<GSDatePickerDelegate>
@property (nonatomic, strong) GSDatePicker *calendar;
@end
